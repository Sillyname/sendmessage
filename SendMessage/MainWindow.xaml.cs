﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using SendMessage.Code;

namespace SendMessage
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Public Properties

        public readonly ObservableCollection<object> source = new ObservableCollection<object>();
        public readonly ObservableCollection<GraphPath> Paths = new ObservableCollection<GraphPath>();

        private double scale = 1.0;
        public double Scale
        {
            get => scale;
            set => SetProperty(ref scale, value);
        }

        private string nodeID;
        public string NodeID
        {
            get => nodeID;
            set => SetProperty(ref nodeID, value);
        }
        private string nodesCount;
        public string NodesCount
        {
            get => nodesCount;
            set => SetProperty(ref nodesCount, value);
        }

        private string edgesCount;
        public string EdgesCount
        {
            get => edgesCount;
            set => SetProperty(ref edgesCount, value);
        }

        private string nodeCount;
        public string NodeCount
        {
            get => nodeCount;
            set => SetProperty(ref nodeCount, value);
        }
        
        private string typeNtwrk;
        public string TypeNtwrk
        {
            get => typeNtwrk;
            set => SetProperty(ref typeNtwrk, value);
        }

        private string nodeFrom;
        public string NodeFrom
        {
            get => nodeFrom;
            set => SetProperty(ref nodeFrom, value);
        }

        private string pathCount;
        public string PathCount
        {
            get => pathCount;
            set => SetProperty(ref pathCount, value);
        }
        
        private string nodeTo;
        public string NodeTo
        {
            get => nodeTo;
            set => SetProperty(ref nodeTo, value);
        }

        private GraphPath selectedPath;
        public GraphPath SelectedPath
        {
            get => selectedPath;
            set
            {
                SetProperty(ref selectedPath, value);

                foreach (var item in pd.Edges)
                {
                    item.IsSelected = false;
                }

                if (selectedPath != null)
                {
                    var path = pd.Paths.First(x => x.Item1 == selectedPath.ID);
                    var nodes = path.Item2.ToList();
                    for (int i = 0; i < path.Item2.Count() - 1; i++)
                    {
                        pd.Edges.Find(edge => edge.Source.nodeID == nodes[i].nodeID && edge.Target.nodeID == nodes[i + 1].nodeID).IsSelected = true;
                    }
                }
            }
        }

        #endregion

        PathDrawer pd = new PathDrawer();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;
            drawPlace.ItemsSource = source;
            listPaths.ItemsSource = Paths;
        }        

        // Метод открытия файла для отображения в окне
        private void OpenFromFile(object sender, MouseButtonEventArgs e)
        {
            //Выбор и чтение файла, инкрементация прогрес бара
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                pd.Nodes.Clear();
                pd.Edges.Clear();
                pd.Paths.Clear();

                source.Clear();

                ProgressBar.Value += (pd.ReadFile(openFileDialog.FileName)) ? 1:0 ;
            }

            // Рассчитаем масштаб
            double maxX = pd.Nodes.Max(x => x.NodeX);
            double maxY = pd.Nodes.Max(x => x.NodeY);

            double scaleX = drawPlace.ActualWidth - 20;
            double scaleY = drawPlace.ActualHeight - 20;

            Scale = CalculateScale(new Size(scaleX, scaleY), new Size(maxX, maxY));

            //Извлечение объектов и отображение посредством ItemDataTemplateSelector   
            foreach (var edge in pd.Edges)
            {
                source.Add(edge);
            }
            foreach (var node in pd.Nodes)
            {
                source.Add(node);
            }

            NodesCount = pd.Nodes.Count.ToString();
            EdgesCount = pd.Edges.Count.ToString();
            TypeNtwrk = "DWDM <- SDH";
        }

        private void DrawPlace_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Ellipse ellipse &&
                ellipse.DataContext is Node node)
            {
                NodeID = node.nodeID.ToString();
            }
            else
            {
                NodeID = "";
            }
        }

        #region INotifyPropertyChanged

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Checks if a property already matches a desired value. Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.</param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            RaisePropertyChanged(propertyName);

            return true;
        }

        /// <summary>
        /// Checks if a property already matches a desired value. Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.</param>
        /// <param name="onChanged">Action that is called after the property value has been changed.</param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        protected virtual bool SetProperty<T>(ref T storage, T value, Action onChanged, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            onChanged?.Invoke();
            RaisePropertyChanged(propertyName);

            return true;
        }

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers
        /// that support <see cref="CallerMemberNameAttribute"/>.</param>
        protected void RaisePropertyChanged([CallerMemberName]string propertyName = null)
        {
            //TODO: when we remove the old OnPropertyChanged method we need to uncomment the below line
            //OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
#pragma warning disable CS0618 // Type or member is obsolete
            OnPropertyChanged(propertyName);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        /// <summary>
        /// Notifies listeners that a property value has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers
        /// that support <see cref="CallerMemberNameAttribute"/>.</param>
        [Obsolete("Please use the new RaisePropertyChanged method. This method will be removed to comply wth .NET coding standards. If you are overriding this method, you should overide the OnPropertyChanged(PropertyChangedEventArgs args) signature instead.")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="args">The PropertyChangedEventArgs</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChanged?.Invoke(this, args);
        }

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <typeparam name="T">The type of the property that has a new value</typeparam>
        /// <param name="propertyExpression">A Lambda expression representing the property that has a new value.</param>
        //[Obsolete("Please use RaisePropertyChanged(nameof(PropertyName)) instead. Expressions are slower, and the new nameof feature eliminates the magic strings.")]
        //[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        //protected virtual void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        //{
        //    var propertyName = PropertySupport.ExtractPropertyName(propertyExpression);
        //    OnPropertyChanged(propertyName);
        //}

        #endregion

        private void DrawPlace_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // Рассчитаем масштаб
            if (pd.Nodes.Count > 0)
            {
                double maxX = pd.Nodes.Max(x => x.NodeX);
                double maxY = pd.Nodes.Max(x => x.NodeY);

                double scaleX = drawPlace.ActualWidth - 20;
                double scaleY = drawPlace.ActualHeight - 20;

                Scale = CalculateScale(new Size(scaleX, scaleY), new Size(maxX, maxY));
            }
        }

        private double CalculateScale(Size destination, Size original)
        {
            double scale = Math.Min(destination.Width / original.Width, destination.Height / original.Height);
            return scale > 1 ? scale : 1 / scale;
        }

        private void TextBlock_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int n = 0;
            NodeCount = "";
            PathCount = "";
            Paths.Clear();
            pd.Paths.Clear();

            foreach (var node in pd.Nodes)
            {
                node.IsSelected = false;
            }
            foreach (var edge in pd.Edges)
            {
                edge.IsChecked = false;
            }

            pd.SearchPath(pd.Nodes[Int32.Parse(NodeFrom)], pd.Nodes[Int32.Parse(NodeTo)]);

            foreach(var path in pd.Paths)
            {
                Paths.Add(new GraphPath(path.Item1, path.Item2));
            }

            PathCount = pd.Paths.Count.ToString();
            foreach (var path in pd.Paths)
            {
                if(path.Item2.Count() > n)
                {
                    n = path.Item2.Count();
                }
            }
            NodeCount = n.ToString();

            pd.Nodes[Int32.Parse(NodeFrom)].IsSelected = true;
            pd.Nodes[Int32.Parse(NodeTo)].IsSelected = true;
        }
    }
}
