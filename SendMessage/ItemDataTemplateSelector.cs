﻿using SendMessage.Code;
using System.Windows;
using System.Windows.Controls;

namespace SendMessage
{
    public class ItemDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate NodeDataTemplate { get; set; }
        public DataTemplate EdgeDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is Node)
                return NodeDataTemplate;
            if (item is Edge)
                return EdgeDataTemplate;

            return base.SelectTemplate(item, container);
        }
    }
}
