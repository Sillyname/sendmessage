﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SendMessage.Converters
{
    [ValueConversion(typeof(bool), typeof(Brush))]
    public class EdgeStrokeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool b)
            {
                if (b)
                    return Brushes.Red;
                return new SolidColorBrush(Color.FromRgb(1, 52, 47));
            }


            throw new ArgumentNullException("Argument must be bool.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
