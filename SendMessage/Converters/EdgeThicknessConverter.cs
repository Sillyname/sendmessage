﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace SendMessage.Converters
{
    [ValueConversion(typeof(bool), typeof(double))]
    public class EdgeThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var t = Int32.Parse(parameter.ToString());
            if (value is bool b)
            {
                if (t == 0)
                {
                    if (b)
                        return 3;
                    return 1.2;
                }
                if (b)
                    return 12;
                return 6;
            }


            throw new ArgumentNullException("Argument must be bool.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
