﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendMessage.Code
{
    interface IParser
    {
        bool TryParse(string filename, IList<Node> nodes, IList<Edge> edges);
    }
}
