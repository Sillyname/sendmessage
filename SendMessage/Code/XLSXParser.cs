﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace SendMessage.Code
{
    public class XLSXParser : IParser
    {
        List<Tuple<string, string>> nodesBefore = new List<Tuple<string, string>>();
        List<Tuple<double, double>> nodesAfter = new List<Tuple<double, double>>();

        public bool TryParse(string filename, IList<Node> nodes, IList<Edge> edges)
        {
            if (String.IsNullOrEmpty(filename))
                throw new ArgumentNullException(nameof(filename), "Путь к файлу не может быть пустым");

            using (ExcelPackage currentPackage = new ExcelPackage(new System.IO.FileInfo(filename)))
            {
                var currentWorksheet = currentPackage.Workbook.Worksheets.First();
                var maxRows = currentWorksheet.Dimension.Rows;
                string currentLine = "";

                for (int i = 2; i <= maxRows; i++)
                {
                    if (!currentWorksheet.Cells[i, 1].Text.TrimEnd(' ').EndsWith(")"))
                    {
                        if (currentWorksheet.Cells[i + 1, 1].Text.StartsWith("LineString"))
                            throw new Exception("Это пиздец, товарищи!");

                        currentLine += currentWorksheet.Cells[i, 1].Text.TrimEnd(' ');
                        continue;
                    }
                    currentLine += currentWorksheet.Cells[i, 1].Text.TrimEnd(' ');

                    var text = currentLine.Split(new char[] { '(', ')' });
                    
                    //Считывание строки из столбца, обрезая всё ненужное по скобкам в строке
                    currentLine = (text.Length > 0) ? text[1] : "";
                    while ((currentLine != null) && (currentLine != "") && (currentLine.Count() > 1))
                    {
                        List<string> tempCoordinates = currentLine.Split(',').ToList();
                        for (int j = 0; j < tempCoordinates.Count; j++)
                        {
                            tempCoordinates[j] = tempCoordinates[j].Remove(0, 1);

                            if (tempCoordinates[j].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length != 2)
                                continue;

                            double NodeX = double.Parse(tempCoordinates[j].Split(' ')[0], CultureInfo.InvariantCulture);
                            double NodeY = double.Parse(tempCoordinates[j].Split(' ')[1], CultureInfo.InvariantCulture);

                            if (!nodesAfter.Exists(node => node.Item1 == NodeX || node.Item2 == NodeY))
                                nodesAfter.Add(new Tuple<double, double>(NodeX, NodeY));
                        }
                        currentLine = "";
                    }
                }
            }
            return true;
        }
    }
}
