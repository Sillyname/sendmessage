﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendMessage.Code
{
    public class GraphPath : BindableBase
    {
        public GraphPath(int id, IEnumerable<Node> nodes)
        {
            ID = id;
            Name = MakeName(nodes);
        }

        private int id;
        public int ID
        {
            get => id;
            set => SetProperty(ref id, value);
        }

        private string name;
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        private string MakeName(IEnumerable<Node> nodes)
        {
            return string.Join(" - ", nodes.Select(n => n.nodeID));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
