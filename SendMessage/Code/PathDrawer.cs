﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SendMessage.Code
{
    //Класс, создающий граф на основе входного текстового файла
    //Также класс конвертирует и вычисляет пути с помощью внешней библиотеки Advanced.Algoritms
    public class PathDrawer
    {
        public List<Node> Nodes = new List<Node>();         //Список считанных узлов
        public List<Edge> Edges = new List<Edge>();         //Список вычесленных связей

        public List<Tuple<int, IEnumerable<Node>>> Paths = new List<Tuple<int, IEnumerable<Node>>>();  //Список вычесленных путей

        //Метод считывания данных из файла, заполняющий внутренние поля класса
        //Сначала производится считывание узлов, затем считывание и вычисление связей вычисление связей
        public bool ReadFile(string filename)
        {
            if(filename.Split(new char[] { '.' }).ToList().Last() == "csv")
            {
                CustomCSVParser CCP = new CustomCSVParser();
                CCP.TryParse(filename, Nodes, Edges);
            }
            else
            {
                XLSXParser XP = new XLSXParser();
                XP.TryParse(filename, Nodes, Edges);
            }

            return true;
        }

        //Метод вызова поиска пути на графе с использованивем внешней библиотеки
        //Возвращаемое значение изменяет поля класса, оставляя лишь требуемые связи и узлы
        public void SearchPath(Node from, Node to)
        {
            AStarAlgorithm aStar = new AStarAlgorithm(new Tuple<List<Node>, List<Edge>>(Nodes, Edges), n => 1);
            int i = 0;
            IEnumerable<Node> current;
            while ((current = aStar.GetPathBetween(from, to)) != null)
            {
                Paths.Add(new Tuple<int, IEnumerable<Node>>(i++, current));
            }
        }
    }
}