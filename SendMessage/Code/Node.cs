﻿namespace SendMessage.Code
{
    //Класс, объясняющий узлы на визуализации и во внутренней логики программы
    public class Node : BindableBase
    {
        public int nodeID;           //Номер узла

        public double NodeX { get; set; }         //Координата Х узла (пикселы)
        public double NodeY { get; set; }         //Координата Y узла (пикселы)

        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value);
        }

        public Node(int nodeID, double nodeX, double nodeY)
        {
            this.nodeID = nodeID;
            this.NodeX = nodeX;
            this.NodeY = nodeY;
        }
    }
}
