﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendMessage.Code
{
    public class CustomCSVParser : IParser
    {
        public bool TryParse(string filename, IList<Node> nodes, IList<Edge> edges)
        {
            int count = 0;
            int j = 0;
            List<Tuple<int, int>> nodePairs = new List<Tuple<int, int>>();

            //Работа с файлом, считывание основной информации, подготовка узлов
            using (StreamReader reader = new StreamReader(filename))
            {
                string currentLine = "";

                //Считывание до конца текстового документа
                while ((currentLine = reader.ReadLine()) != null)
                {
                    List<double> buffer = new List<double>();

                    //Заполнение буфера связующех точек и создание узла
                    IList<double> items = currentLine.Split(new char[] { ';' }).Select(x => Double.Parse(x)).ToList();
                    nodes.Add(new Node(count, items[0], items[1]));
                    buffer.AddRange(items.Skip(2));

                    //Заполнение списка пар точек для преобразования в ребро
                    while (buffer.Count != 0)
                    {
                        nodePairs.Add(new Tuple<int, int>(count, (int)buffer.First()));
                        buffer.RemoveAt(0);
                    }

                    count++;
                }

                count = 0;
            }

            //Преобразования списка пар узлов в ребра
            foreach (Tuple<int, int> item in nodePairs)
            {
                edges.Add(new Edge(j,
                          Math.Sqrt(
                          Math.Pow(nodes[item.Item2].NodeX - nodes[item.Item1].NodeX, 2) + Math.Pow(nodes[item.Item2].NodeY - nodes[item.Item1].NodeY, 2)),
                          item.Item1,
                          item.Item2,
                          nodes[item.Item1],
                          nodes[item.Item2]));
                j++;
            }

            return true;
        }
    }
}
