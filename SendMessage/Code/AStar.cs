﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SendMessage.Code
{
    //Алгоритм нахождения пути в графе (заебался всех напрягать и сам ебаться с внешними библиотеками. только я знаю, как должны работать мои классы, ссука, заебало)
    internal class AStarAlgorithm
    {
        //Хранение переданного графа и весов узлов графа (состоящего ТОЛЬКО из МОИХ элементов)
        private readonly Tuple<List<Node>, List<Edge>> graph;
        private readonly Func<Node, double> g;
        public AStarAlgorithm(Tuple<List<Node>, List<Edge>> graph, Func<Node, double> g)
        {
            this.graph = graph;
            this.g = g;
        }

        //Поиск пути между двумя узлами, возвращающий перечень узлов, как путь (ГОСПАДИ, это можно будет НОРМАЛЬНО отрисовать)
        public IEnumerable<Node> GetPathBetween(Node from, Node to)
        {
            //Перечни "открытых" и "закрытых" узлов (по каким "прошли", а по которым "предстоит пройти")
            List<Node> closedNodes = new List<Node>(0);
            List<Node> openNodes = new List<Node>() { from };

            //Перечни временных характеристик G, H и F для нужного количества узлов [Это основная формула алгоритма -> f(x) = g(x) + h(x) (где Я решу, ЧТО там будет лежать)]
            Dictionary<Node, double> g = new Dictionary<Node, double>();
            Dictionary<Node, double> h = new Dictionary<Node, double>();
            Dictionary<Node, double> f = new Dictionary<Node, double>();

            //Список для восстановления пути
            Dictionary<Node, Node> parent = new Dictionary<Node, Node>();
            foreach (Node node in graph.Item1) { parent.Add(node, null); }


            //Результирующий граф
            List<Node> pathResult = new List<Node>(0);

            //Заполнение характеристик стартовой вершины
            startNode();

            /*Основное тело алгоритма, обработка последующих точек*/

            //Пока не высвободится список узлов "требующих рассмотрения"
            while (openNodes.Count != 0)
            {
                //Выбор узла с минимальной эвристикой (ЕСЛИ ДО НЕГО МОЖНО ДОЙТИ)
                Node currentNode = openNodes.First(node => node == f.OrderBy(x => x.Value).First().Key);

                //Проверка на достижимость конечного узла
                if (currentNode == to)
                {
                    break;
                }

                //Если в открытых узлах есть такой элемент - удалить его, т.к. он находится в обработке
                if (openNodes.Any(x => x.nodeID == currentNode.nodeID))
                {
                    openNodes.RemoveAll(node => node.nodeID == currentNode.nodeID);
                }

                //Вне зависимости от предыдущего решения, добавить узел в список просмотренных
                closedNodes.Add(currentNode);
                f.Remove(currentNode);

                //Обзор ближайших соседей текущего узла
                foreach (Node closestNode in graph.Item2.Where(edge => edge.NodeIDFrom == currentNode.nodeID && !edge.IsChecked).Select(x => x.Target))
                {
                    double tentativeScore = heuristicCosteStimate(currentNode, closestNode) + g[currentNode];
                    if (closedNodes.Exists(node => node == closestNode) && tentativeScore >= g[closestNode])
                    {
                        continue;
                    }

                    if (!closedNodes.Exists(node => node == closestNode) || tentativeScore < g[closestNode])
                    {
                        //Добавление предшевствующего узла для восстановлении пути
                        parent[closestNode] = currentNode;

                        if (!g.ContainsKey(closestNode) || !h.ContainsKey(closestNode) || !f.ContainsKey(closestNode))
                        {
                            g.Add(closestNode, tentativeScore);
                            h.Add(closestNode, heuristicCosteStimate(closestNode, to));
                            f.Add(closestNode, g[closestNode] + h[closestNode]);
                        }
                    }

                    if (!openNodes.Exists(node => node.nodeID == closestNode.nodeID))
                    {
                        openNodes.Add(closestNode);
                    }
                }

            }

            //Восстановление пути по словарю
            ReconstructPath(parent);

            return pathResult.Exists(node => node == from) ? pathResult : null;

            //Восстановление пути по словарю
            List<Node> ReconstructPath(Dictionary<Node, Node> parents)
            {
                Node currentNode = to;
                while (currentNode != null)
                {
                    pathResult.Add(currentNode);
                    currentNode = parents[currentNode];
                }

                pathResult.Reverse();

                //Маркировка "использованных" путей
                UpdateEdges(pathResult);

                return pathResult;
            }

            //Заполнение значений стартовой вершины
            void startNode()
            {
                g.Add(from, 0);
                h.Add(from, heuristicCosteStimate(from, to));
                f.Add(from, g[from] + h[from]);
            }

            //Эвристическая функция оценки расстояния
            double heuristicCosteStimate(Node From, Node To)
            {
                return graph.Item2.Exists(x => (x.NodeIDFrom == from.nodeID && x.NodeIDTo == to.nodeID) || (x.NodeIDFrom == to.nodeID && x.NodeIDTo == from.nodeID)) ?
                       graph.Item2.Find(x => (x.NodeIDFrom == from.nodeID && x.NodeIDTo == to.nodeID) || (x.NodeIDFrom == to.nodeID && x.NodeIDTo == from.nodeID)).edgeLenght :
                       Math.Sqrt(Math.Pow(Math.Abs(from.NodeX - to.NodeX), 2) + Math.Pow(Math.Abs(from.NodeY - to.NodeY), 2));
            }
        }

        //Маркировка "использованных" путей
        public void UpdateEdges(List<Node> falsingEdges)
        {
            for (int i = 1; i < falsingEdges.Count; i++)
            {
                if (graph.Item2.Exists(edge => (edge.Source == falsingEdges[i - 1]) && (edge.Target == falsingEdges[i])))
                {
                    graph.Item2.Find(edge => (edge.Source == falsingEdges[i - 1]) && (edge.Target == falsingEdges[i])).IsChecked = true;
                }

                if (graph.Item2.Exists(edge => (edge.Target == falsingEdges[i - 1]) && (edge.Source == falsingEdges[i])))
                {
                    graph.Item2.Find(edge => (edge.Target == falsingEdges[i - 1]) && (edge.Source == falsingEdges[i])).IsChecked = true;
                }
            }
        }

    }
}