﻿namespace SendMessage.Code
{
    //Класс, объясняющий связи на визуализации и во внутренней логике программы
    public class Edge : BindableBase
    {
        public int edgeID;             //Номер связи
        public double edgeLenght;      //Длина соединения

        public int NodeIDFrom { get; set; }      //Номер узла ОТ
        public int NodeIDTo { get; set; }        //Номер узла ДО

        private bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set => SetProperty(ref isSelected, value);
        }

        private bool isChecked;
        //Участие связи в маршруте доставки
        public bool IsChecked
        {
            get => isChecked;
            set => SetProperty(ref isChecked, value);
        }

        public Node Source { get; set; }
        public Node Target { get; set; }

        public Edge(int edgeID, double edgeLenght, int nodeIDFrom, int nodeIDTo, Node source, Node target, bool isChecked = false)
        {
            this.edgeID = edgeID;
            this.edgeLenght = edgeLenght;
            this.NodeIDFrom = nodeIDFrom;
            this.NodeIDTo = nodeIDTo;
            this.IsChecked = isChecked;

            this.Source = source;
            this.Target = target;
        }
    }
}
